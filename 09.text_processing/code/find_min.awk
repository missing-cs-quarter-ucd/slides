function find_min_assignment() {
    min_val = $5
    for (i = 6; i <= NF; i++)
        if ($i < min_val)
            min_val = $i
    return min_val
}

NR > 1 {
    print $2" "$3"'s lowest score was "find_min_assignment()
}