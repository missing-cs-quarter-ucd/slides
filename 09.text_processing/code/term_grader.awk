#!/usr/bin/awk -f

BEGIN{
    FS = ","
    class_score = 0
}

function find_min() {
    min_val = $5
    for (i = 6; i <= NF; i++)
        if ($i < min_val)
            min_val = $i
    return min_val
}

NR > 1 {
    student_score = 0
    for (i = 5; i<= NF; i++)
        student_score += $i
    student_score -= find_min()
    student_score /= 3
    
    class_score += student_score
    print $2" "$3": "student_score
}

END{
    print "\nClass Average: " class_score / NR
}