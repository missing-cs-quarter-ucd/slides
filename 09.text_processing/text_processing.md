[% INCLUDE "template.md" title="Text Processing" author="Stephen Ott" %]
[% BLOCK slides %]

---

# Agenda

- How to manipulate files using `sed`
- How to parse and manipulate data using `awk`
- Tradeoffs between `grep`, `sed`, and `awk`

---

# Problem scenario

As an instructor, you are responsible for managing the student roster and grades, stored in a csv file

.center[
![:scale 95%](images/csv_sample.png)
. . .
]

Three tasks
1. Drop a student from the course
2. Add a student to the course
3. Replace all `MISSING` scores with zeroes

---


layout: true

# Sed

---

## Stream editor

- Operates on streams
  * Can be piped into
  * By default outputs to stdout
- Allows for filtering, substitution, addition, and deletion of text from file
- Two ways to run

.footnotesize.lcol[
```terminal
$ sed [-e] '<sed_command>' <target_file>
```
]
.footnotesize.rcol[
```terminal
$ sed -f <sed_script_file> <target_file>
```
]
![:flush 1]

- sed will only modify a file if given the `-i` flag

--

## The anatomy of a sed command

.small[

```
<range_begin>,<range_end>/<regexp>/<command> <arg_1>/...<arg_n>/<modifier>
```

]

- `range_begin`, `range_end`, and `regexp` are optional conditions called addresses
  * Need to apply the `-E` flag to use ERE regex
- `command` is a single character which may have arguments
- `modifier` modifies how the command gets executed.

???

- Everything before the first slash is optional.
  * The command will not be executed on a particular line if that line is not in the range or does not match the regex
- We'll showcase a few of the most common sed commands as we go

---

## Making sed act like grep

- `p` command prints a line
- `-n` flag suppresses lines that didn't fit conditions

.small[
```terminal
$ sed -n '/,3,/p' gradebook.csv | head
2864302586,Sigfried,Speares,3,73.41,90.36,93.26,81.12
0100947093,Lena,Gibbin,3,98.42,78.93,78.77,74.05
2928102213,Eve,McGoldrick,3,76.53,96.31,95.33,90.63
9495381495,Kathlin,Bollands,3,94.12,88.7,79.96,87.85
1207520055,Kane,Taggerty,3,85.29,92.25,85.78,89.82
3297779721,Hyman,France,3,80.68,77.91,87.18,75.69
6633911203,Marleen,Tesyro,3,82.76,79.51,76.99,89.05
8001108716,Rose,Huc,3,MISSING,80.3,MISSING,103.8
7353573139,Celene,Matteacci,3,85.38,92.33,89.7,73.73
9222435206,Venita,Lundbeck,3,77.08,100.82,92.24,MISSING
```
]

--

## Printing certain line numbers

.small[
```terminal
$ sed -n '2,6p' gradebook.csv 
2864302586,Sigfried,Speares,3,73.41,90.36,93.26,81.12
1851748393,Clint,McKain,2,82.06,88.39,79.11,81.38
6349370635,Phedra,Randerson,1,86.34,85.2,78.32,MISSING
6409527799,Ansell,Ewestace,1,90.53,80.86,78.91,78.3
8536727527,Dale,Hannah,2,82.73,88.34,72.0,77.07
```
]

---

## Deleting lines with sed

- `d` command removes a line

.small[
```terminal
$ grep -n 'Noah' gradebook.csv 
102:1234567890,Noah,Rose,3,50.30,65.48,62.3,58.5
$ tail -n 3 gradebook.csv                
7765780379,Sybil,Verlinde,2,85.64,93.27,MISSING,74.15
0928658422,Merilee,Edwicker,2,88.92,76.43,79.66,101.99
1234567890,Noah,Rose,3,50.30,65.48,62.3,58.5
*$ sed '/Noah/ d' gradebook.csv | tail -n 3
3951079142,Jana,Tapscott,1,92.04,MISSING,88.61,79.29
7765780379,Sybil,Verlinde,2,85.64,93.27,MISSING,74.15
0928658422,Merilee,Edwicker,2,88.92,76.43,79.66,101.99
```
]

--

- This did not change the file

.small[
```terminal
$ sed -i '/Noah/ d' gradebook.csv
$ tail -n 3 gradebook.csv
3951079142,Jana,Tapscott,1,92.04,MISSING,88.61,79.29
7765780379,Sybil,Verlinde,2,85.64,93.27,MISSING,74.15
0928658422,Merilee,Edwicker,2,88.92,76.43,79.66,101.99
```
]

---

## Appending lines with sed

- `a` command appends a line of text after each line matching the address

.small[
```terminal
$ new_student='4567890123,Grant,Gilson,3,MISSING,MISSING,MISSING,MISSING'
$ echo $new_student 
4567890123,Grant,Gilson,3,MISSING,MISSING,MISSING,MISSING
*$ sed "1a $new_student" gradebook.csv | head -n3
student_id,first_name,last_name,year,assignment_1,assignment_2,assignment_3,assignment_4
4567890123,Grant,Gilson,3,MISSING,MISSING,MISSING,MISSING
2864302586,Sigfried,Speares,3,73.41,90.36,93.26,81.12
```
]

???

- Ask at the end if the new student is reflected in the csv file

--

- This also did not modify the file

.small[
```terminal
$ sed -i "1a $new_student" gradebook.csv
$ head -n3 gradebook.csv
student_id,first_name,last_name,year,assignment_1,assignment_2,assignment_3,assignment_4
4567890123,Grant,Gilson,3,MISSING,MISSING,MISSING,MISSING
2864302586,Sigfried,Speares,3,73.41,90.36,93.26,81.12
```
]

---

## Substituting strings with sed

- Probably the most common use for sed
- Breaks from our standard form for a command
  * `sed 's/<find_string>/<replace_string>/<option>' <file>`

.small[
```terminal
$ grep -c MISSING gradebook.csv
34
*$ sed 's/MISSING/0/g' gradebook.csv | grep -c ,0
34

```
]

???

- At this point, I've mentioned it a few times, but because we didn't use the `-i` flag
  the original csv file is unaltered

---

## A new one-line hope

- We can execute multiple sed commands at once

.small[
```terminal
$ sed -i -e '/Noah/ d' -e "1a $new_student" -e 's/MISSING/0/g' gradebook.csv
```
]

![:newline]

--

## The `-f` flag strikes back

.small.lcol[
[% bashfile("gb_modifier.sed", "3..5") %]
]

.small.rcol[
```terminal
$ sed -f gb_modifier.sed gradebook.csv
```
]

![:flush 1]

--

## Return of the shebang line

.small.lcol[
[% bashfile("gb_modifier.sed") %]
]

.small.rcol[
```terminal
$ chmod +x gb_modifier.sed
$ ./gb_modifier.sed gradebook.csv
```
]

---

layout: false

# Calculating final grades

- Drop the students lowest assignment
- Average all of the scores to compute the students' grades
- Calculate the average score of the class

.center.small[
| First Name | Last Name  | Adjusted Overall Grade |
|:----------:|:----------:|:----------------------:|
| Richmond   |	Petegrew  | ???                    |
| Jordan     |	Bottjer   |	???                    |
| Pancho     |	Dressel   |	???                    |
| Quinta     |	Cremen    |	???                    |
| Shurlock   |	Cleveley  |	???                    |
]

---

layout: true

# Awk

---

## The awk programming language

- Processes commands for each line in a file
- Each line called a record
- Each component of a line called a field
- Fields appear as variables referenced in order of appearance

.center.footnotesize[
| $1         | $2   | $3   | $4 | $5    | $6    | $7   | $8   |     
|:----------:|:----:|:----:|:--:|:-----:|:-----:|:----:|:----:|
| 1234567890 | Noah | Rose | 3  | 50.30 | 65.48 | 62.3 | 58.5 |
]

???

- Somewhat helpful to think of awk as being useful for when you notice data is in a table format

--

## Special Variables

.center.footnotesize[
| Variable Name |             Variable Meaning            |
|:-------------:|:---------------------------------------:|
| FS            | Field Separator. Defaults to whitespace |
| NF            | Number of fields in the current record  |
| NR            | Number of the current record            |
]

---

## How to use awk

- Two ways to use

.footnotesize[
.lcol[
```terminal
$ awk '<awk_commands>' <target_file>
```
]

.rcol[
```terminal
$ awk -f <awk_script_file> <target_file>
```
]
]

- Change the field separator with an argument to the `-F` flag 
- awk has no `-i` flag to modify files in place

![:flush 1]

--

## The anatomy of an awk command

.small[
```terminal
<condition> { <awk_code> }
```
]

- Code will only be executed for all lines that satisfy the condition
  * Code will be executed for all lines if no condition is given
- This condition can be a regex in the form of `/<regex_patter>/`

---

## Selective printing using awk

- Just use `print var` to print a variable

.small[
```terminal
$ awk -F ',' '{ print $3 }' processed_gradebook.csv | head -n5
last_name
Gilson
Speares
McKain
Randerson
```
]

--

.small[
```terminal
$ awk -F ',' 'NR != 1 { print $3","$2 }' processed_gradebook.csv | head -n5
Gilson,Grant
Speares,Sigfried
McKain,Clint
Randerson,Phedra
Ewestace,Ansell
```
]

---
## Conditionals

- Use if-else statements to have branching logic

--

.footnotesize[
```terminal
$ awk -F ',' '{ if ($4 == 1 && $5 < 70) print $2" "$3" got a failing score on their first assignment" }' processed_gradebook.csv
Lori Brammall got a failing score on their first assignment
```
]

--

![:newline]

.footnotesize[
[% srcfile("awk", "freshman_that_failed.awk") %]
]

.footnotesize[
```terminal
$ awk -F ',' -f freshman_that_failed.awk processed_gradebook.csv 
Lori Brammall got a failing score on their first assignment
```
]

---

## Everything that has a beginning, has an end

- Use the `BEGIN` and `END` keywords to specify blocks to run before and after processing

.small[
[% srcfile("awk","number_as.awk") %]
]

.small[
```terminal
$ awk -F ',' -f number_as.awk processed_gradebook.csv
26 A's for assignment 2
```
]

---

## For loops

- Similiar syntax as C for loops

.small[
[% srcfile("awk","for_loop_example.awk") %]
]

.small[
```terminal
$ awk -F ',' -f for_loop_example.awk processed_gradebook.csv | head -n5
Grant Gilson: 0
Sigfried Speares: 84.5375
Clint McKain: 82.735
Phedra Randerson: 62.465
Ansell Ewestace: 82.15
```
]

---

## Functions

- Use the `function` keywords and list arguments without types

.footnotesize[
[% srcfile("awk", "find_min.awk") %]

```terminal
$ awk -F ',' -f find_min.awk processed_gradebook.csv | tail -n5 
Marlie Crichten's lowest score was 78.79
Gardie Gabbott's lowest score was 72.37
Jana Tapscott's lowest score was 0
Sybil Verlinde's lowest score was 0
Merilee Edwicker's lowest score was 76.43
```

]

---

## Bringing it all together

--

.footnotesize[[% bashfile("term_grader.awk", "3..-1") %]]


---

layout: false

# `sed` vs `awk` vs `grep`

![:newline]

## `grep`

- Useful when you extract data that matches a pattern
- Cannot do any data modification

--

## `sed`

- Useful for transforming data
- Can modify files in place
- Cannot perform advanced computation

--

## `awk`

- Capable of anything `sed` or `grep` can do
- More expressive
- More typing
- Cannot modify files in place effectively

---


# Conclusion

- `sed` and `awk` are great utilities for modifying data
- By utilizing regex, we can leverage these powerful utilities
- Manipulating and filtering data empowers you to solve problems in new and concise ways

---

# References & Further Reading
- `sed` and `awk` man pages
- [GNU `sed` manual](https://www.gnu.org/software/sed/manual/sed.html)
- [GNU `awk` manual](https://www.gnu.org/software/gawk/manual/gawk.html)
  * Despite being quite vanilla, these resources provide more than you'll care to know
- [`sed` tutorial](https://www.digitalocean.com/community/tutorials/the-basics-of-using-the-sed-stream-editor-to-manipulate-text-in-linux)
  * I liked how concise this tutorial
- [`awk` tutorialspoint](https://www.tutorialspoint.com/awk/index.htm)
  * Lots of good examples

[% END %]
