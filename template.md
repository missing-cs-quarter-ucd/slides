[% INCLUDE "utils/template.html" %]
[% BLOCK content %]
[% PROCESS "utils/tt2/srcfile.tt" %]

class: center, middle
name: title

# ECS 98F - [% title %]

![:newline]

.Large[*[% author %]*]

![:newline 3]

![:scale 50%]([% utils %]/style/UCD_CS_Logo.jpg)

.footnote[Copyright © 2020-2021 Grant Gilson, Stephen Ott, Joël Porquet-Lupine,
Aakash Prabhu, Noah Rose Ledesma  
[CC BY-NC-SA 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/)]

[% PROCESS slides %]

[% END %]
