#ifndef USERNAME_VALIDATOR_H
#define USERNAME_VALIDATOR_H

#include <stdbool.h>

bool IsUsernameValid(const char *username);
bool HasNoSpecialChars(const char *username);
bool DoesNotStartWithNumber(const char *username);
bool MeetsLengthRequirement(const char *username);
bool IsNotReserved(const char *username);

#endif // USERNAME_VALIDATOR_H
