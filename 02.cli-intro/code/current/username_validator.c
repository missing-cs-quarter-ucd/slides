/*
 * Username Validator
 */
#include <ctype.h>
#include <string.h>

#include "username_validator.h"

/*
 * Returns true if a username is valid, false otherwise.
 * A username is valid if and only if the following are true:
 * 1) The username contains no special characters. That is, it may only
 * contain letters and digits.
 * 2) The username does not start with a number.
 * 3) The username contains at least three and no more than ten
 * characters.
 * 4) The username is not one of "admin" or "noah"
 */
bool IsUsernameValid(const char *username) {
  return HasNoSpecialChars(username) && DoesNotStartWithNumber(username)
	  && MeetsLengthRequirement(username) && IsNotReserved(username);
}

/*
 * Internal functions
 */
bool HasNoSpecialChars(const char *username) {
  while(*username != '\0') {
    if(!isalnum(*username))
      return false;
    username++;
  }
  return true;
}

bool DoesNotStartWithNumber(const char *username) {
  return !isdigit(*username);
}

bool MeetsLengthRequirement(const char *username) {
  int length = strlen(username);
  return length >= 3 && length < 10;
}

bool IsNotReserved(const char *username) {
  if(strcmp(username, "admin") == 0) {
    return false;
  }
  if (strcmp(username, "noah") == 0) {
    return false;
  }
  return true;
}
