#include <CUnit/Basic.h>
#include "username_validator.h"

void TestHasNoSpecialChars() {
  // Positive exampes
  CU_ASSERT(HasNoSpecialChars("abcdefghijklmnopqrstuvwxyz"));
  CU_ASSERT(HasNoSpecialChars("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
  CU_ASSERT(HasNoSpecialChars("1234567890"));
  CU_ASSERT(HasNoSpecialChars(""));
  // Negative examples
  CU_ASSERT(!HasNoSpecialChars("!@#$%^&*()"));
  CU_ASSERT(!HasNoSpecialChars("[]\\{}|;':\",./<>?"));
  CU_ASSERT(!HasNoSpecialChars("AbbyR0ad!"));
  CU_ASSERT(!HasNoSpecialChars(" "));
  CU_ASSERT(!HasNoSpecialChars("D@rk S!de ()f Th3 M()()n"));
}

void TestDoesNotStartWithNumber() {
  // Positive Examples
  CU_ASSERT(DoesNotStartWithNumber("foobar"));
  CU_ASSERT(DoesNotStartWithNumber("bang0"));
  CU_ASSERT(DoesNotStartWithNumber("th3 wall"));
  // Negative Examples
  CU_ASSERT(!DoesNotStartWithNumber("0Joe"));
  CU_ASSERT(!DoesNotStartWithNumber("9St4r"));
}

void TestMeetsLengthRequirement() {
  // Positive Examples
  CU_ASSERT(MeetsLengthRequirement("abc"));
  CU_ASSERT(MeetsLengthRequirement("****"));
  CU_ASSERT(MeetsLengthRequirement("0!0!0"));
  CU_ASSERT(MeetsLengthRequirement("0jo0jo0jo"));
  CU_ASSERT(MeetsLengthRequirement("1234567890"));
  // Negative examples
  CU_ASSERT(!MeetsLengthRequirement(""));
  CU_ASSERT(!MeetsLengthRequirement("1"));
  CU_ASSERT(!MeetsLengthRequirement(" P"));
  CU_ASSERT(!MeetsLengthRequirement("12345678901"));
  CU_ASSERT(!MeetsLengthRequirement("1234567890AAAAAAAAAAAAAAAA"));
}

void TestIsNotReserved() {
  // Positive examples
  CU_ASSERT(IsNotReserved("foobar"));
  CU_ASSERT(IsNotReserved("!!0"));
  CU_ASSERT(IsNotReserved(" "));
  CU_ASSERT(IsNotReserved("\0"));
  CU_ASSERT(IsNotReserved("9999"));
  // Negative examples
  CU_ASSERT(!IsNotReserved("admin"));
}

void ExampleFailingTest() {
  CU_ASSERT(DoesNotStartWithNumber("0"));
}

int main()
{
    // Initialize CUnit
    CU_initialize_registry();
    CU_pSuite pSuite = CU_add_suite("UsernameValidatorTests", NULL, NULL);

    // Register unit tests
    CU_add_test(pSuite, "HasNoSpecialChars", TestHasNoSpecialChars);
    CU_add_test(pSuite, "DoesNotStartWithNumber", TestDoesNotStartWithNumber);
    CU_add_test(pSuite, "MeetsLengthRequirement", TestMeetsLengthRequirement);
    CU_add_test(pSuite, "IsNotReserved", TestIsNotReserved);

    // Run all tests
    CU_basic_run_tests();

    // Clean up & exit
    CU_cleanup_registry();
    return 0;
}