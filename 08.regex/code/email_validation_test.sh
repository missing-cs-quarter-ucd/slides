#!/bin/bash

pattern=$1

read email

if [[ "$email" =~ $pattern ]]; then
    echo "$email is a valid email address"
else
    echo "$email is an invalid email address"
fi
