#!/bin/bash

phone_number=$1

if [[ "$phone_number" =~ ^([0-9]{3})-([0-9]{3})-([0-9]{4})$ ]]; then
    echo "Phone number: ${BASH_REMATCH[0]}"
    echo "Area code: ${BASH_REMATCH[1]}"
    echo "Prefix: ${BASH_REMATCH[2]}"
    echo "Line number: ${BASH_REMATCH[3]}"
else
    echo "Invalid phone numnber"
    exit 1
fi
