#!/usr/bin/python

import re
from sys import argv

email = argv[1]
match = re.match(r'^([\w#-]+)@([[:alnum:]-]+)\.([^\W_]{2,4})$', email)

if match:
  print(f"Username: {match.group(1)}")
  print(f"Second-level domain: {match.group(2)}")
  print(f"Top-level domain: {match.group(3)}")
else:
  print("Invalid email")
