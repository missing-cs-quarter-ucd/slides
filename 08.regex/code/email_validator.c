#define PCRE2_CODE_UNIT_WIDTH 8
#include <stdio.h>
#include <pcre2.h>
#include <string.h>

int main(int argc, char **argv) {
  PCRE2_SIZE erroffset;
  int errcode;
  uint32_t options = 0, ovecsize = 128;
  const char* patt = "^([\\w#-]+)@([[:alnum:]-]+)\\.([^\\W_]{2,4})$";
  const char* email = argv[1];
  size_t patt_size = strlen(patt), subject_size = strlen(email);

  pcre2_code* re = pcre2_compile(patt, patt_size, options, &errcode, &erroffset, NULL);
  pcre2_match_data* match_data = pcre2_match_data_create(ovecsize, NULL);
  int rc = pcre2_match(re, email, subject_size, 0, options, match_data, NULL);

  if (rc > 0) {
    const char *lables[] = {"Username", "Second-level domain", "Top-level domain"};
    PCRE2_SIZE* ovector = pcre2_get_ovector_pointer(match_data);
    for (PCRE2_SIZE i = 1; i < rc; i++) {
      PCRE2_SPTR start = email + ovector[2*i];
      PCRE2_SIZE slen = ovector[2*i+1] - ovector[2*i];
      printf("%s: %.*s\n", lables[i - 1], (int)slen, (char *)start);
    }
  }
  else if (rc <= 0) 
    printf("Invalid email\n");

  pcre2_match_data_free(match_data);
  pcre2_code_free(re);
}
