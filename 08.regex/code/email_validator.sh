#!/bin/bash

read email

if [[ "$email" =~ ^[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]{2,4}$ ]]; then
    echo "$email is a valid email"
else
    echo "$email is not a valid email"
    exit 1
fi
