#!/bin/bash

email=$1

if [[ "$email" =~  ^([[:word:]#-]+)@([[:alnum:]-]+)\.([[:alnum:]]{2,4}$) ]]; then
    echo "Username: ${BASH_REMATCH[1]}"
    echo "Second-level domain: ${BASH_REMATCH[2]}"
    echo "Top-level domain: ${BASH_REMATCH[3]}"
else
    echo "$email is not a valid email"
    exit 1
fi
