[% INCLUDE "template.md" title="Regex" author="Stephen Ott" %]
[% BLOCK slides %]

---

layout: true

# Agenda

---

## Today's lecture

- How we can robustly look for patterns in strings
- How we can extract data from strings

???

- 

---

layout: true

# Problem scenario

---

## Validating email addresses

You have to design a script to tell whether or not an email address is valid.

### Rules for a valid email

- Username
  * Can contain any alpahnumeric character and the speical characters `_`, `-`, and `#`
  * Cannot contain `.`
- Username and Doman must be seperated by a `@`.
  * This must be the only `@` in the string
- Domain
  * Can contain any alphanumeric character and `-`
- Top-level domain
  * Must contain between 2 and 4 alphanumeric characters


???

- Note that this is not the full standard for email validation. I've simplified the rules a bit for the sake of keeping our examples relatively simple
- Pause the video and consider how you might approach this problem with the tools that you already know

---

## What we want

```terminal
$ echo bob_smith123@gmail.com | email_validator.sh
bob_smith123@gmail.com is a valid email address
```

```terminal
$ echo .no@h.rose@@!#.h | email_validator.sh
.no@h.rose@@!#.h is an invalid email address
```


```terminal
$ for i in email_list.txt; do
    echo $i | email_validator.sh
done
```

---

layout: true

# Intro to Regular Expressions

---

.large[A regular expression is a string encoding a “pattern” that matches some set of strings]

???

- This defintion is taken from Professor Dave Doty's ECS 120 lectures
  * You'll learn a more mathmatical way to think about regex in that class
- For a string to match a regex, said string must fit the pattern that the regex defines

--

.lcol[
## What you can do with regex

- Find strings
- Validate strings
- Extract strings
- Substitute strings

## Where to use regex

- The bash language
- `grep`
- `sed` and `awk`
- Standard library of any programming language
]

.rcol[
  .center[
  ![:scale 100%](images/relevant_xkcd.jpg)
]
]
???

- The glob star and other bash wildcard characters are a diet version of regex
- You have been using grep up until this point to match literal substrings, but now we'll learn how to extend its capability
- We'll talk about sed and awk next lecture
- Any mainstream programming language you come across has an implementation of a regex engine that you can use right out of the box

---

## A simple bash script

 <!-- TODO: Investigate why the bashfile macro isn't working here -->

[% bashfile("email_validation_test.sh") %]

- `=~` checks if the string on the left matches the regex pattern on the right

```terminal
$ echo foo | ./email_validation_test.sh foobar
foo is an invalid email address
$ echo foobar | ./email_validator_test.sh foobar
foobar is a valid email address
```

???

- Here, we're going to employee a practice of writing code to just experiment
  * This is a helpful way to explore solutions to a problem that applies to not just this problem

---

layout: true

# Regex Grammar

---

## Bread and butter

.center.small[
| Token | Description                         |
|:-----:|:-----------------------------------:|
| `.`   | Matches with *any* single character |
| `a*`  | Matches zero or more `a`'s          |
| `a+`  | Matches one or more `a`'s           |
]

???
- Actually pause the video and try to come up with a pattern using these constructs to solve the problem

--

```terminal
$ echo 'bob_smith123@gmail.com' | ./email_validation_test.sh '.*@.*\..*'
bob_smith123@gmail.com is a valid email address
```

--

```terminal
$ echo '.@.@@.@' | ./email_validation_test.sh '.*@.*\..*' 
.@.@@.@ is a valid email address
```

--

```terminal
$ echo '@.' | ./email_validation_test.sh '.*@.*\..*' 
@. is a valid email address
```

???
- As you can see, this alone matches entirely too much, allowing a user to violate most the rules we've set forth
- We need to be able to restrict characters in different parts of the string

---


## Character sets

- Matches with any character in the set
- Ranges, i.e. `a-z` and `0-5` are acceptable

.small[

```terminal
$ echo 'stephen123@yahoo.com' | ./email_validation_test.sh '[a-zA-Z0-9_#-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+'
stephen123@yahoo.com is a valid email address
```

```terminal
$ echo '.@.@@.@' | ./email_validation_test.sh '[a-zA-Z0-9_#-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+'
.@.@@.@ is an invalid email address
```

]

???

- This looks to work, but this is getting pretty long. Can we shorten it?
--

## Negated character sets

- Matches with any character not in the set

.small[

```terminal
$ echo 'bob_123@gmail.com' | ./email_validation_test.sh '[^@.]+@[^@.]+\.[^@.]'       
bob_123@gmail.com is a valid email address
```

```terminal
$ echo '.@.@@.@' | ./email_validation_test.sh '[^@.]+@[^@.]+\.[^@.]'
.@.@@.@ is an invalid email address
```

```terminal
$ echo '!!$tephens mail@yahoo.com' | ./email_validation_test.sh '[^@.]+@[^@.]+\.[^@.]'
!!$tephens mail@yahoo.com is a valid email address
```

]

???

- This doesn't do what we want since this pattern expresses that we want a character that isn't
  `@` or `.`, followed by `@`, followed by another character that isn't `@` or `.` followed by `.com`

---

## Meta sequences

.center.footnotesize[
| Token       | Description                      | Character Set Equivalence    |
|:-----------:|:--------------------------------:|:----------------------------:|
| `[:digit:]` | matches any single digit         | `[0-9]`                      |
| `[:alpha:]` | matches any alphabetic character | `[A-Za-z]`                   |
| `[:alnum:]` | matches any letter or digit      | `[A-Za-z0-9]`                |
| `[:word:]`  | `[:alnum:]` but with `_`         | `[A-Za-z0-9_]`               |
| `[:space:]` | matches any whitespace character | `[ \t\n\r]`                  |
]

- These belong inside square brackets

.small[
```terminal
$ echo '.@.@@.@' | ./email_validation_test.sh '[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]+'
.@.@@.@ is an invalid email address
```

```terminal
$ echo 'bob_smith123@gmail.com' | ./email_validation_test.sh '[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]+'
bob_smith123@gmail.com is a valid email address
```
]

--

.small[
```terminal
$ echo '@tephens mail@yahoo.com' | ./email_validation_test.sh '[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]+'
@tephens mail@yahoo.com is a valid email address
```
]


---

## Anchors

Tokens that represent the start and end of the string

.center.small[
| Token | Description            |
|:-----:|:----------------------:|
| `^`   | Start of the string    |
| `$`   | End of the string      |
]

.small[

```terminal
$ echo '@tephens mail@yahoo.com' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]+$'
@tephens mail@yahoo.com is an invalid email address
```
]

--

.small[
```terminal
$ echo 'bob_smith123@gmail.commmmmmmmmmmmmmmmmmmm' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]+$'
bob_smith123@gmail.commmmmmmmmmmmmmmmmmmm is a valid email address
```
]

---

## Logical or

.center.small[
| Token | Description                    |
|:-----:|:------------------------------:|
| `a∣b` | Matches with either `a` or `b` |
]

.small[
```terminal
$ echo 'bob_smith123@gmail.commmmmmmmmmmmmmmmmmmm' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.(com|org|edu|gov)$'
bob_smith123@gmail.commmmmmmmmmmmmmmmmmmm is an invalid email address
```

```terminal
$ echo 'bob_smith123@gmail.fake_domain' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.(com|org|edu|gov)$'
bob_smith123@gmail.fake_domain is an invalid email address
```
]

--

.small[
```terminal
$ echo 'bob_smith123@gmail.info' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.(com|org|edu|gov)$'
bob_smith123@gmail.info is an invalid email address
```
]

---

## Quantifiers

.center.small[
| Token    | Description                                  |
|:--------:|:--------------------------------------------:|
| `a{n}`   | Matches exactly 3 `a`'s                      |
| `a{m,n}` | Matches between `m` and `n` `a`'s, inclusive |
| `a{m,}`  | Matches `m` or more `a`'s                    |
| `a{,n}`  | Matches no more than `n` `a`'s               |
]

.small[

```terminal
$ echo 'bob_smith123@gmail.commmmmmmmmmmmmmmmmmmm' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]{2,4}$'
bob_smith123@gmail.commmmmmmmmmmmmmmmmmmm is an invalid email address
```

```terminal
$ echo 'bob_smith123@gmail.com' | ./email_validation_test.sh '^[[:word:]#-]+@[[:alnum:]-]+\.[[:alnum:]]{2,4}$' 
bob_smith123@gmail.com is a valid email address

```

]

---

## Revisiting email validation

- Tweak our script that we've been using so far to accept a candidate email

???

- At this point, pause the video, and try to come up with what that regex should be yourself
- What does an email look like?

--

.small[[% bashfile("email_validator.sh") %]]

---

layout: true

# Extracting strings

---

- Now, you have been tasked with extracting a user's username, second-level domain, and top-level
  domain.

.center[
![:scale 100%](images/email_captures.svg)
]

---

## Capture groups

- Parentheses allow specification of a substring to be later referenced
- Stored in `$BASH_REMATCH` as an array after the use `=~`
  * `${BASH_REMATCH[0]}` stores the whole match
  * Indices `1, 2, ... n` store captures in order of appearance

???

- Acutally pause the video and try to come up with 

--

.small[

[% bashfile("capture_group_example.sh") %]

```terminal
$ ./capture_group_example.sh 123-456-7890
Phone number: 123-456-7890
Area code: 123
Prefix: 456
Line number: 7890
```
]

---

## The final script

.small[[% bashfile("email_extractor.sh") %]]

---

layout: true

# Regex flavors

---

### POSIX Extended Regular Expressions (ERE)

- Strict superset of BRE
- What we have used thus far with bash's `=~` operator
- Special characters are special by default

--

### POSIX Basic Regular Expressions (BRE)

- Default dialect used by many shell utilities, i.e. `sed` and `grep`
- Special characters like `+` and `{}` have to be escaped with `\`

--

### Perl-Compatible Regular Expressions (PCRE)

- Many more powerful and flexible features than however similiar to the other two
- API's exist for use of this in C, Python, and other languages

???

- I cannot tell you how many times I've been using `grep` or some other shell utility that uses 
  regex and have been befuddled because the syntax I was using was not supported by that utility's 
  default regex engine.

--

![:newline]

.footnotesize.center[
<table>
  <tr>
    <th>BRE</th>
    <td><code>^\([[:word:]#-]\+\)@\([[:alnum:]-]\+\)\.\([[:alnum:]]\{2,4\}\)$</code></td>
  </tr>
  <tr>
    <th>ERE</th>
    <td><code>^([[:word:]#-]+)@([[:alnum:]-]+)\.([[:alnum:]]{2,4})$</code></td>
  </tr>
  <tr>
    <th>PCRE</th>
    <td><code>^([\w#-]+)@([[:alnum:]-]+)\.([^\W_]{2,4})$</code></th>
  </tr>
</table>
]

---

## More on PCRE

- Comes with shorthands for frequently used character groups
  * Shorthands can be negated with capitalization

.center.small[
| Shorthand | BRE/ERE Equivalent | Character Set Equivalent |
|:---------:|:------------------:|:------------------------:|
| `\w`      | `[:word:]`         | `[A-Za-z_]`              |
| `\d`      | `[:digit:]`        | `[0-9]`                  |
| `\s`      | `[:space:]`        | `[ \n\t\r]`              |
]

- Allows for non-capturing groups using `(?:...)`
- Lookaround constructs provide ability to make assertions about strings without assertions being included in the match

---

layout: true


# More on grep

---

## grep was made for regex

- Use flags to utilize a full regex grammar
  * `-E` for ERE
  * `-P` for PCRE
  * No flag to use BRE

## More useful flags

.center.footnotesize[
| Flag      | Function                                                                |
|:---------:|:------------------------------------------------------------------------|
| `-v`      | Select line which don't match the given pattern                         |
| `-l`      | List files which contain a match for the regex                          |
| `-i`      | Ignore case when doing the match                                        |
| `-c`      | Print the number of matching lines in each input file                   |
| `--color` | Prints matches with colors to highlight what part of the string matched |
| `-n`      | Print each matching line with the line number it came from              |
| `-r`      | Run recursively for all files                                           |
]

---

## Data wrangling with grep

---

layout: false

# The Ubiquity of Regex


## Python

.footnotesize[[% bashfile("email_validator.py") %]]

---

## C

.footnotesize[[% cfile("email_validator.c") %]]


---

## Conclusion

- Regex is a powerful tool that is used everywhere
- Regex can greatly many simplify parsing problems

---

layout: false

# Helpful links

- [Regex 101](https://regex101.com/)
  * Explains different tokens of the regex grammar and allows you to experiment
- [Regex One](https://regexone.com/)
  * Interactive tutorial useful for building up intuition for constructing regex
- [Regex Cheatsheet](https://remram44.github.io/regex-cheatsheet/regex.html#syntax)
  * Good overall cheatsheet. Also provides a nice comparisson between regex flavors
- [A Regular Expression Matcher](https://www.cs.princeton.edu/courses/archive/spr09/cos333/beautiful.html)
  * Not necessary to learn regex, but demonstrates that the implementation of a regex engine can be quite simple
- [Regular Expression in C](https://lloydrochester.com/post/c/regex_pcre/)
  * Blog post about using PCRE in C
- David Doty's ECS 120
  * Not a link, but this class provides a mathmatical basis for regular expressions and what they are capable of

[% END %]
