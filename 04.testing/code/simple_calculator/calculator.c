/*
 * Simple calculator program.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int calculator(int lhs, const char *operator, int rhs) {
    if(strcmp(operator, "+") == 0) {
        return lhs + rhs;
    } else if(strcmp(operator, "-") == 0) {
        return lhs - rhs;
    } else if(strcmp(operator, "*") == 0) {
        return lhs * rhs;
    } else if(strcmp(operator, "/") == 0) {
        return lhs / rhs;
    }
}

int main(int argc, char **argv) {
    if(argc != 4) {
        printf("Useage: %s lhs operator rhs\n", argv[0]);
        return 1;
    }

    printf("%i\n", calculator(atoi(argv[1]), argv[2], atoi(argv[3])));
    return 0;
}