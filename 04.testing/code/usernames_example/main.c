#include <stdio.h>
#include "username_validator.h"

int main(int argc, char** argv) {
  if(argc != 2) {
    printf("Useage: %s <username>\n", argv[0]);
    return 1;
  }

  if(IsUsernameValid(argv[1])) {
    printf("%s is a valid username\n", argv[1]);
    return 0;
  } else {
    printf("%s is not a valid username\n", argv[1]);
    return 1;
  }
}
