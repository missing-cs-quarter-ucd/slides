/*
 * Program to test the username validator
 */
#include <stdio.h>

#include "username_validator.h"

#define NUM_TESTS 12 

struct pair{
  char *username;
  bool should_be_valid;
};

struct pair input_output_pairs[NUM_TESTS] = {
  // Valid usernames
  { .username="john42", .should_be_valid=true},
  { .username="SarahsCool", .should_be_valid=true},
  { .username="DeadB33f", .should_be_valid=true},
  // Invalid -- contains special chars
  { .username="Uno!", .should_be_valid=false},
  { .username="y*o", .should_be_valid=false},
  { .username="@#$%^&*", .should_be_valid=false},
  // Invalid -- starts with a number
  { .username="1boss", .should_be_valid=false},
  { .username="28Hugo99", .should_be_valid=false},
  // Invalid -- too short or too long
  { .username="ma", .should_be_valid=false},
  { .username="A1234567890", .should_be_valid=false},
  // Invalid -- is one of the restricted names
  { .username="admin", .should_be_valid=false},
  { .username="noah", .should_be_valid=false},
};

// Given the index of an input-output pair, run the test on the username
// validator. Returns true if the tests passes, false otherwise.
bool run_test(int test_num) {
  const char *input = input_output_pairs[test_num].username;
  bool real_output = IsUsernameValid(input);
  bool expected_output = input_output_pairs[test_num].should_be_valid;
  if(real_output != expected_output) {
    printf("Test %i failed! Expected %s to %sbe a valid username\n", test_num + 1,
            input, expected_output ? "" : "not ");
    return false;
  }
  return true;
}

int main(int argc, char **argv) {
  int num_passed = 0;
  for(int i = 0; i < NUM_TESTS; i++) {
    if(run_test(i))
      num_passed++;
  }
  printf("%i out of %i tests passed.\n", num_passed, NUM_TESTS);
}
