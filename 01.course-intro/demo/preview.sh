
# Deps 
# grip - https://pypi.org/project/grip/
# restview - https://pypi.org/project/restview/
# firefox - https://www.mozilla.org/

preview(){
	case $1 in
		*.md)
			grip --pass 'redacted' $1
			;;

		*.rst)
			restview $1		
			;;

		*.html)
			firefox $1
			;;
		*.pdf)
			firefox $1
			;;
		*.svg)
			firefox $1
			;;
		*)
			echo "Unsupported File type $1, consider adding it to the script!"

	esac
}

preview $1
