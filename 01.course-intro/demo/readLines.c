#include <stdio.h> 
#include <stdlib.h> 
#include "readLines.h"

void 
read_lines(FILE* fp, char*** lines, int* num_lines){
    int fileLength = 0, stringIndex = 0;
    fseek(fp, 0L, SEEK_END);
		    int sz = ftell(fp);
    rewind(fp);
    int String_Length[sz];
    char c;
    while (!feof(fp)){
        c = fgetc(fp);
        fileLength++;
        if(c == '\n'){
String_Length[stringIndex] = fileLength;
            stringIndex++;                         
fileLength = 0;
            (*num_lines)++;
        }
    }
    (*lines) = (char**)calloc(*num_lines, sizeof(char*));
    for(int i = 0; i < *num_lines; i++)(*lines)[i] = (char*) calloc(String_Length[i]+1, sizeof(char));
fseek(fp, 0, SEEK_SET);
    for(int i = 0; i < *num_lines; i++){
fgets((*lines)[i],(String_Length[i] +1), fp);
    }
    return;
}
