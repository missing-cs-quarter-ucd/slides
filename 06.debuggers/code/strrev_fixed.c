/*
 * A (broken) program to reverse a string
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char STR[] = "Hello World!";

int main(int argc, char** argv) {
  char* reversed = malloc(strlen(STR));
  int reversed_index = strlen(STR) - 1;
  unsigned int str_index = 0;

  while(reversed_index >= 0) {
    reversed[reversed_index] = STR[str_index];
    reversed_index--;
    str_index++;
  }

  printf("%s\n", reversed);
  free(reversed);

  return 0;
}
