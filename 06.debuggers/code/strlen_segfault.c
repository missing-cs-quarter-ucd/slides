#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

size_t foo_len (const char *s)
{
	assert(s && "String cannot be NULL here!");
    return strlen(s);
}

int main (int argc, char *argv[])
{
    char *a = NULL;

    printf ("size of a = %ld\n", foo_len(a));

    return 0;
}
