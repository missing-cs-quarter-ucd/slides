/*
 * Implmentation of the Caesar shift cipher
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char* cipher(const char* plaintext, int shift) {
  char* ciphertext = calloc(sizeof(char),strlen(plaintext));
  
  for(int i = 0; plaintext[i] != '\0'; i++) {
    if(isalpha(plaintext[i])) {
      char plain_char = toupper(plaintext[i]);
      printf("Plain char: %c\n", plain_char);
      int index = plain_char - 'A';
      printf("Index: %i\n", index);
      int shifted_index = (index + shift) % 26;
      printf("Shifted index: %i\n", shifted_index);
      char shifted_char = shifted_index + 'A';
      printf("Shifted char: %c\n\n", shifted_char);
      ciphertext[i] = shifted_char;
    } else {
      ciphertext[i] = plaintext[i];
    }
  }

  return ciphertext;
}

char* decipher(const char* ciphertext, int shift) {
  char* plaintext = calloc(sizeof(char),strlen(ciphertext));

  for(int i = 0; ciphertext[i] != '\0'; i++) {
    if(isalpha(ciphertext[i])) {
      char cipher_char = toupper(ciphertext[i]);
      int index = cipher_char - 'A';
      int shifted_index = (index - shift) % 26;
      while(shifted_index < 0) {
        shifted_index += 26;
      }
      char plaintext_char = shifted_index + 'A';
      plaintext[i] = plaintext_char;
    } else {
      plaintext[i] = ciphertext[i];
    }
  }

  return plaintext;
}

int main(int argc, char** argv) {
  if(argc != 4) {
    printf("Useage: %s <action> <text> <shift>\n", argv[0]);
    printf("Action may be one of 'e' or 'd' for encode or decode.\n");
    return 1;
  }

  if(*argv[1] == 'e') {
    char* ciphertext = cipher(argv[2], atoi(argv[3]));
    printf("%s\n", ciphertext);
    free(ciphertext);
  } else if (*argv[1] == 'd') {
    char* plaintext = decipher(argv[2], atoi(argv[3]));
    printf("%s\n", plaintext);
    free(plaintext);
  } else {
    printf("Unrecognized action\n");
    return 0;
  }

  return 1;
}
