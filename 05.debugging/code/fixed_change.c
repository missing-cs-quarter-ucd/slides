/*
* Program to determine the fewest number of bills required to sum to an arbitrary target.
* This program contains a bug on 17 where the value is incorrectly compared with a strict inequality.
*/
#include <stdio.h>
#include <stdlib.h>

void MakeChange(int value) {
    unsigned int ones = 0, tens = 0, fifties = 0, hundreds = 0;
    while(value > 0) {
        if(value >= 100) {
            hundreds++;
            value -= 100;
        } else if(value >= 50) {
            fifties++;
            value -= 50;
        } else if(value >= 10) {
            tens++;
            value -= 10; 
        } else if(value >= 1) {
            ones++;
            value -= 1;
        }
    }
    printf("The change is:\n");
    printf("%i x $100\n", hundreds);
    printf("%i x $50\n", fifties);
    printf("%i x $10\n", tens);
    printf("%i x $1\n", ones);
}

int main(int argc, char** argv) {
    if(argc != 2) {
        printf("Useage: %s <dollar amount>\n", argv[0]);
        return 1;
    }
    MakeChange(atoi(argv[1]));
}