/*
* Program to determine the fewest number of bills required to sum to an arbitrary target.
* This program contains a bug on 17 where the value is incorrectly compared with a strict inequality.
*/
#include <stdio.h>
#include <stdlib.h>

void MakeChange(int value) {
    unsigned int ones = 0, tens = 0, fifties = 0, hundreds = 0;
    while(value > 0) {
        if(value >= 100) {
            hundreds++;
            value -= 100;
            printf("Gave $100, new value: %i\n", value);
        } else if(value >= 50) {
            fifties++;
            value -= 50;
            printf("Gave $50, new value: %i\n", value);
        } else if(value > 10) {
            tens++;
            value -= 10; 
            printf("Gave $10, new value: %i\n", value);
        } else if(value >= 1) {
            ones++;
            value -= 1;
            printf("Gave $1, new value: %i\n", value);
        }
    }
}

int main(int argc, char** argv) {
    if(argc != 2) {
        printf("Useage: %s <dollar amount>\n", argv[0]);
        return 1;
    }
    MakeChange(atoi(argv[1]));
}