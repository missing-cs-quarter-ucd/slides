#!/bin/bash

mode=$1
find=$2
replace=$3
# Shift 3, so $@ will refer to the target files
shift 3
files=$@

print_help()
{
   # Prints a help message to the user
   echo "Usage: ./batch_rename.sh [-h|-d|-f] FIND REPLACE FILES"
   echo ""
   echo "FIND: The substring that constitutes a file name we want to change"
   echo "REPLACE: What we will replace the FIND string with"
   echo "FILES: The target files to be renamed"
   echo "   -h: Help. Prints this help message and exits"
   echo "   -d: Dry run. Prints what the changes would be but does not execute the changes"
   echo "   -f: Force. Changes the file names in place."
}

do_rename()
{
   dry_run=$1
   # For each file
   for i in $files
   do
      # If $find is a substring of $i
      if [[ $i == *"${find}"* ]] 
      then
         # perform the substitution
         new_name=${i/$find/$replace}

         # if the user indicated they don't want to do a dry run
         if [[ $dry_run = false ]]
         then
            # perform the rename
            mv $i $new_name && echo "Successfully renamed $i to $new_name"
         else
            # else just print what the result would be
            echo "Would rename $i to $new_name"
         fi
      fi
   done
}

case $mode in
   '-h')
      print_help
      ;;
   '-d')
      echo 'Performing dry run'
      do_rename true
      ;;
   '-f')
      echo 'Performing rename in place'
      do_rename false
      ;;
   *)
      echo 'Invalid arguments. Try again'
      print_help
      exit 1
esac
