#!/bin/bash

grep 'Gandalf' list_of_dwarves.txt

if [[ $? != 0 ]]
then
  echo "Gandalf is not a dwarf"
  exit 1
else
  echo "Gandalf is a dwarf"
  exit 0
fi