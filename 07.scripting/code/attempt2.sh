#!/bin/bash

for i in IMG*.jpg
do
    new_file_name=${i/IMG/image}
    echo "Renaming $i to $new_file_name"
    mv $i $new_file_name
done