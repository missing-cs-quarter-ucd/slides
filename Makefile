## Source directories
src += 01.course-intro
src += 02.cli-intro
src += 03.cli-advanced
src += 04.testing
src += 05.debugging
src += 06.debuggers
src += 07.scripting
src += 08.regex
src += 09.text_processing
src += 10.git

## Extra dependencies
dep := template.md

## Include rules
include utils/Rules.mk
